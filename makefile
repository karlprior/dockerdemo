build:
	cd clienttools && make build
	cd hpcc && make build
	cd jenkins && make build

volume:
	docker volume create --name DataVolume1

inspectvolume:
	docker volume inspect DataVolume1

compose:
	docker-compose up

stopall:
	docker stop $(docker ps -a -q)

removeall:
	docker rm $(docker ps -a -q)

removeallimages:
	docker rmi $(docker images -q) --force