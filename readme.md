# Docker presentation
## Run

```bash
docker-compose up
```

You may need to run HPCC from interactive mode. Cd to hpcc directory:

```bash
docker run -it --rm -p 8010:8010 odin/hpcc /bin/bash
```

When logged into HPCC:

```bash
service init-hpcc start
```

